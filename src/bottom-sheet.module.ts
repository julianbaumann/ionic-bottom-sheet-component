import { CommonModule } from "@angular/common";
import { NgModule, ModuleWithProviders } from "@angular/core";
import { BottomSheetComponent } from "./components/bottom-sheet-component";
import { IonicModule } from "@ionic/angular";

@NgModule({
    imports: [
        IonicModule,
        CommonModule
    ],
    declarations: [
        BottomSheetComponent
    ],
    exports: [
        BottomSheetComponent
    ]
})

export class BottomSheetModule {
    static forRoot(): ModuleWithProviders
    {
        return { ngModule: BottomSheetComponent }
    }
}