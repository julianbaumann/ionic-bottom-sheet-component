import { Component, ElementRef, EventEmitter, Input, Output, ChangeDetectorRef } from "@angular/core";

const HTML_TEMPLATE = `
<div class="BottomSheetBackdrop" id="BottomSheetBackdrop"></div>

<div class="BottomSheetOuter" id="BottomSheetOuter" (click)="ChangeBottomSheetStateToClosed()" (scroll)="ScrollChanged($event)">
    <div class="BeforeMainBottomSheetBox"></div>
    <div class="BeforeMainBottomSheetBox CustomStateBox"></div>
    <div id="BottomSheet" class="BottomSheetBox" (click)="PreventClose($event)">
        <hr *ngIf="ShowIndicator">
        <div class="BottomSheetContent" [ngClass]="{'BottomSheetWithoutIndicator': !ShowIndicator}"><ng-content></ng-content></div>
    </div>
</div>
`;

const CSS_STYLE = `
.BottomSheetBackdrop
{
    position: absolute;
    height: 100%;
    width: 100vw;
    top: 0;
    left: 0;
    background-color: black;
    overflow: hidden;
    opacity: 0;
    visibility: hidden;
    transition: 0.3s;
    z-index: 5;
    cursor: pointer;
}

.BottomSheetOuter
{
    position: fixed;
    height: 200vh;
    width: 100vw;
    z-index: 100;
    top: 200%;
    left: 0;
    overflow: hidden;
    overflow-y: auto;
    scroll-snap-type: y mandatory;
    overflow: -moz-scrollbars-none;
    -ms-overflow-style: none;
    visibility: hidden;
    transition: 0.3s ease-out;
    cursor: pointer;
}

.ShowBottomSheetOuter
{
    top: 0;
    transition: top 0.3s ease;
    visibility: visible;
}

.ShowBackdrop
{
    opacity: 0.7;
    visibility: visible;
}

.BottomSheetOuter::-webkit-scrollbar
{
    -webkit-appearance: none;
    width: 0;
    height: 0;
}

.BottomSheetOuter::-webkit-scrollbar
{
    display: none;
}

.BeforeMainBottomSheetBox
{
    background-color: transparent;
    width: 100vw;
    scroll-snap-align: start;
    height: 100vh;
}

.CustomStateBox
{
    height: 0;
}

.BottomSheetBox
{
    background-color: white;
    scroll-snap-align: center;
    width: 100vw;
    height: auto;
    padding-bottom: 100vh;
    border-top-left-radius: 15px;
    border-top-right-radius: 15px;
    color: var(--ion-text-color);
    cursor: default;
}

hr
{ 
    width: 35px; 
    margin: 0 auto;
    margin-top: 10px;
    margin-bottom: 10px;
    height: 4px;
    border-radius: 5px;
    display: inline-block;
    background: #595858;
    position: relative;
    left: 50%;
    transform: translateX(-50%);
}

.BottomSheetContent
{
    overflow: scroll;
    max-height: 90vh;
    -ms-overflow-style: block;
}

.Fullscreen
{
    height: 90vh;
}

.BottomSheetWithoutIndicator
{
    max-height: 94vh;
}

@media (prefers-color-scheme: dark)
{
    .BottomSheetBox
    {
        background-color: #1C1C1E;
        color: white;
    }
    hr
    {
        background: #6C6C6D;
    }
}

@media only screen and (min-width: 768px)
{
    .BottomSheetOuter
    {
        position: fixed;
        height: 100vh;
        width: 100vw;
        z-index: 100;
        top: 200%;
        left: 0;
        overflow: hidden;
        visibility: hidden;
        transition: 0.5s ease-out;
    }

    .BeforeMainBottomSheetBox
    {
        display: none;
    }

    .BottomSheetBox
    {
        width: 600px;
        height: 600px;
        position: relative;
        top: 50%;
        left: 50%;
        padding: 0;
        transform: translate(-50%, -50%);
        border-radius: 15px;
        overflow: scroll;
    }

    hr
    {
        display: none;
    }

    .ShowBottomSheetOuter
    {
        top: 0;
        transition: top 0.3s;
        visibility: visible;
    }

    .BottomSheetContent
    {
        max-height: none;
    }

    .Fullscreen
    {
        height: 600px
    }
}
`

@Component({
    selector: "bottom-sheet",
    template: HTML_TEMPLATE,
    styles: [CSS_STYLE],
    outputs: [ "State" ]
})
export class BottomSheetComponent
{
    private static randomString: string;
    private static wasTriggeredRecently = false;
    private _state: SheetStates = SheetStates.Closed;

    @Input()
    public get State()
    {
        return this._state;
    }
    public set State(value: SheetStates)
    {
        if (!BottomSheetComponent.wasTriggeredRecently)
        {
            this._state = value;
            this.StateChange.emit(value);
            if (value == SheetStates.Opened || value == SheetStates.Docked)
            {
                this.ShowBottomSheet();
            }
            else
            {
                this.CloseBottomSheet();
            }
            BottomSheetComponent.wasTriggeredRecently = true;
            setTimeout(() =>
            {
                BottomSheetComponent.wasTriggeredRecently = false;
            }, 200);
        }
    }

    @Input("ShowIndicator") public ShowIndicator: boolean = true;
    @Input("ShowBackdrop") public ShowBackdrop: boolean = true;

    @Input("Fullscreen") public Fullscreen: boolean = false;
    @Input("DockedHeight") public DockedStateHeight: number = 0;


    @Output() public StateChange: EventEmitter<SheetStates> = new EventEmitter<SheetStates>();
    @Output() public ScrollChange: EventEmitter<SheetStates> = new EventEmitter<SheetStates>();

    constructor(private _elementRef: ElementRef) {}

    private async ShowBottomSheet()
    {
        const SheetPresenter = this._elementRef.nativeElement;

        BottomSheetComponent.randomString = await this.MakeId(10);
        const ionRouter = (<HTMLElement> window.document.querySelector("ion-router-outlet"));

        SheetPresenter.setAttribute("id", `SheetPresenter${BottomSheetComponent.randomString}`);
        SheetPresenter.getElementsByClassName("BottomSheetBackdrop")[0].id = `BottomSheetBackdrop${BottomSheetComponent.randomString}`;
        SheetPresenter.getElementsByClassName("BottomSheetOuter")[0].id = `BottomSheetOuter${BottomSheetComponent.randomString}`;

        await ionRouter.parentNode.insertBefore(SheetPresenter, ionRouter);

        const BottomSheetOuter = (<HTMLElement> window.document.getElementById(`BottomSheetOuter${BottomSheetComponent.randomString}`));
        const Backdrop = (<HTMLElement> window.document.getElementById(`BottomSheetBackdrop${BottomSheetComponent.randomString}`));
        const Content = (<HTMLElement> window.document.getElementsByClassName("BottomSheetContent")[0]);
        if (this.Fullscreen)
        {
            Content.classList.add("Fullscreen");
        }
        const CustomStateBox = (<HTMLElement> window.document.getElementsByClassName("CustomStateBox")[0]);
        CustomStateBox.style.height = (<string> this.DockedStateHeight.toString() + "px");

        if (this.State == SheetStates.Opened)
        {
            this.OpenBottomSheet(BottomSheetOuter);
        }
        else if (this.State == SheetStates.Docked)
        {
            this.DockBottomSheet(BottomSheetOuter);
        }


        BottomSheetOuter.classList.add("ShowBottomSheetOuter");

        if (!this.ShowBackdrop)
        {
            Backdrop.style.opacity = "0";
        }

        Backdrop.classList.add("ShowBackdrop");
    }

    private OpenBottomSheet(bottomSheetOuter: HTMLElement)
    {
        bottomSheetOuter.scrollTop = document.body.scrollHeight + this.DockedStateHeight;
    }

    private DockBottomSheet(bottomSheetOuter: HTMLElement)
    {
        bottomSheetOuter.scrollTop = document.body.scrollHeight;
    }

    private CloseBottomSheet()
    {
        try
        {
            const BottomSheetOuter = (<HTMLElement> window.document.getElementById(`BottomSheetOuter${BottomSheetComponent.randomString}`));
            const Backdrop = (<HTMLElement> window.document.getElementById(`BottomSheetBackdrop${BottomSheetComponent.randomString}`));

            if (BottomSheetOuter != null)
            {

                BottomSheetOuter.classList.remove("ShowBottomSheetOuter");
                Backdrop.classList.remove("ShowBackdrop");

                setTimeout(async() =>
                {
                    BottomSheetOuter.scrollTop = 0;
                    const SheetPresenter = await document.querySelector(`bottom-sheet#SheetPresenter${BottomSheetComponent.randomString}`);
                    const ionApp = SheetPresenter.parentElement;
                    if (SheetPresenter != undefined)
                    {
                        try
                        {
                            await ionApp.removeChild(SheetPresenter);
                        }
                        catch (e)
                        {
                            console.log("ERROR", e);
                        }
                    }
                }, 100);
            }
        }
        catch (e)
        {
            console.error(`ERROR IN BOTTOM-SHEET COMPONENT WHILE DISMISSING CARD: ${e}`);
        }
    }

    private MakeId(length)
    {
        let result = "";
        const characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        const charactersLength = characters.length;
        for (let i = 0; i < length; i++) {
           result += characters.charAt(Math.floor(Math.random() * charactersLength));
        }
        return result;
    }

    public ChangeBottomSheetStateToClosed()
    {
        this.State = SheetStates.Closed;
    }

    public PreventClose(event)
    {
        event.stopPropagation();
    }

    public ScrollChanged(event)
    {
        const Content = event.target.getElementsByClassName("BottomSheetContent")[0];

        this.ScrollChange.emit(event);

        const backdropElement = event.target.previousElementSibling;

        const windowHeight = window.outerHeight;
        const scrollPosition = (event.target.scrollTop) / (windowHeight);

        if (Content != null && backdropElement != null)
        {
            if (event.target.scrollTop <= windowHeight)
            {
                backdropElement.style.transition = "0s";
                backdropElement.style.opacity = scrollPosition - 0.3;
            }
        }

        if (scrollPosition - 0.1 <= 0)
        {
            this.State = SheetStates.Closed;
        }
    
        if (event.target.scrollTop == document.body.scrollHeight && Content != null)
        {
            BottomSheetComponent.wasTriggeredRecently = true;
            this.State = SheetStates.Docked;
            Content.style.overflowY = "hidden";

            this.StateChange.emit(this.State);
            setTimeout(() =>
            {
                BottomSheetComponent.wasTriggeredRecently = false;
            }, 200);
        }
        else if (Content != null)
        {
            Content.style.overflowY = "scroll";
        }

        const OpenedHeight = event.target.scrollHeight - (document.body.scrollHeight * 2);

        if (event.target.scrollTop == OpenedHeight)
        {
            BottomSheetComponent.wasTriggeredRecently = true;
            this.State = SheetStates.Opened;

            this.StateChange.emit(SheetStates.Opened);
            setTimeout(() =>
            {
                BottomSheetComponent.wasTriggeredRecently = false;
            }, 200);
        }
    }

}

export enum SheetStates
{
    Closed = 0,
    Docked,
    Opened
}
