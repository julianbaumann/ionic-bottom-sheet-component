# Ionic Bottom Sheet Component

<br />
<br />
<br />

# === DEPRECIATED ===
# DON'T USE IT ANYMORE.
# IT IS *NOT* RECOMMENDED FOR PRODUCTION.
# IT WILL BE DELETED SOON
<br />
<br />
<br />
<br />
<br />
<br />
<br />



A simple component for ionic 4/5 that brings up a full customizable bottom sheet.

<div align="center">
<p align="center">
<img align="center" src="https://gitlab.com/julianbaumann/ionic-bottom-sheet-component/raw/master/Docked.gif" width="200"/>
</p>
</div>

## Angular 9 Problems

There seems to be an problem with angular 9. If you receive an error like ```ERROR in getInternalNameOfClass() called on a non-ES5 class: expected BottomSheetComponent to have an inner class declaration``` try import the dist folder like ```ionic-custom-bottom-sheet/dist``` instead of just ```ionic-custom-bottom-sheet```

## Installation
```shell
npm install ionic-custom-bottom-sheet@latest --save
```

Import it to your `app.module.ts` file.
```ts
import { BottomSheetComponent, BottomSheetModule } from "ionic-custom-bottom-sheet";

@NgModule({
    declarations: [AppComponent],
    entryComponents: [BottomSheetComponent],
    imports: [
        ...
        BottomSheetModule
        ...
    ],
    ...
})
```

Since Ionic uses lazy loading you also have to import it to your page module. As example if we want to use it on our home-page.

In home.module.ts:
```ts
import { BottomSheetModule } from "ionic-custom-bottom-sheet";

@NgModule({
  imports: [
    ...
    BottomSheetModule,
    ...
})
```

## Usage
Just write your content in between the `<bottom-sheet> </bottom-sheet>` tag. 

```html
<button (click)="OpenSheet()">Open Bottom Sheet</button>

<bottom-sheet [(State)]="BottomSheetState" (StateChange)="StateChanged($event)">
    <p style="margin-top: 30%; margin-bottom: 30%; text-align: center"> 
        Hello World 
    </p>
</bottom-sheet>
```

```ts
import { SheetStates } from "ionic-custom-bottom-sheet";
...
export class HomePage
{

    public BottomSheetState: SheetStates = SheetStates.Closed;

    constructor() {}

    public OpenSheet()
    {
        this.BottomSheetState = SheetStates.Opened;
    }

    public StateChanged(event)
    {
        if (event == SheetStates.Closed)
        {
            console.log("Sheet Closed");
        }
    }
}
```

> This code in action shown below

<div align="center">
<p align="center">
<img src="https://gitlab.com/julianbaumann/ionic-bottom-sheet-component/raw/master/Preview.gif" width="200" />
</p>
</div>

<br />
<br />

## SheetStates
- Closed
- Docked
- Opened

## Fullscreen

<div align="center">
<p align="center">
<img src="https://gitlab.com/julianbaumann/ionic-bottom-sheet-component/raw/master/Fullscreen.gif" width="200" />
</p>
</div>
<br />

> The size of the sheet will be the same as its content height, as long as it isn't bigger than 90vh, if so it will stay at 90vh and make its content scrollable. You can also set `[Fullscreen]="true"` to make it always stay at 90vh.


<br />
<br />

## Docked State

<div align="center">
<p align="center">
<img src="https://gitlab.com/julianbaumann/ionic-bottom-sheet-component/raw/master/Docked.gif" width="200" />
</p>
</div>

<br />

> In the example shown above `[DockedHeight]` is set to `400` and `[Fullscreen]` is set to `true`. `[DockedHeight]` is the distance in pixels from the top position.

## Attributes
|   Attribute     |    Type      |     Default        |
|-----------------|--------------|--------------------|
| [Fullscreen]    | boolean      | false              |
| [ShowIndicator] | boolean      | true               |
| (StateChange)   | Event        |                    |
| [(State)]       | SheetStates  | SheetStates.Closed |
| (ScrollChange)  | Event        |                    |
| [DockedHeight]  | number in px | 0                  |
| [ShowBackdrop]  | boolean      | true               |

<br />
<br />

> Since it looks weird on bigger screens like on a tablet or computer screen, the Sheet transforms into a modal like box.

<div align="center">
<p align="center">
<img src="https://gitlab.com/julianbaumann/ionic-bottom-sheet-component/raw/master/TabletView.gif" width="900" />
</p>
</div>

## Styling

> To overwrite styles, I would recommend using the `.BottomSheetBox` Class, here you can overwrite things like the border-radius, color, background-color etc. You have to use it in your `variables.scss` and also add `!important` after each statement.
